local move_x, move_y = 0, 0 
local flag_x, flag_y = 0, 0
ballSize = 20
local ENEMY_HP = 20
local MY_HP = 1
local pex = require("pex")
particle = pex.load("animation/clear.pex","animation/clearTexture.png")
resultTextView = nil
isEnd, textEnd = 0, 0 
local addBall = function (x, y, isEnemy) 
	local ball = display.newCircle( x, y, ballSize)
	if isEnemy == 1 then
		print("Enemy")
		ball:setFillColor(1,0,0,0.8)
	else 
		print("Not Enemy")
		ball:setFillColor(1,1,1,0.8)
	end
	return ball
end
field = {}
speed = 12.5
transition.animText = function(subj, prop)
	local retTrans
	if type(subj) == "table" and type(subj.text) == "string" and type(subj._proxy) == "userdata" then
		local index = {idx=0}
		local origText = subj.text
		local runtimeAdded = function()
			if(prop.addRandText ~= false) then
				subj.text = origText:sub(1,index.idx)..math.random(48,122)
			else
				subj.text = origText:sub(1,index.idx)
			end
		end
		local onCancelled = function()
			if prop.onCancelled ~= nil then
				prop.onCancelled(prop)
			end
			subj.text = origText
			Runtime:removeEventListener("enterFrame",runtimeAdded)
		end
		local onComplete = function()
			onCancelled()
			if prop.onComplete ~= nil then
				prop.onComplete(prop)
			end
			Runtime:removeEventListener("enterFrame",runtimeAdded)
		end
		local onStart = function()
			if prop.onStart ~= nil then
				prop.onStart(prop)
			end
			Runtime:addEventListener("enterFrame",runtimeAdded)
		end
		retTrans = transition.to(index,{time=(prop.time or 500),idx=subj.text:len(),onCancelled=onCancelled,onComplete=onComplete,easing=prop.transition,delay=prop.delay,onStart=onStart,onRepeat=prop.onRepeat})
		subj.text = ""
	end
	return retTrans
end

local signInv = function(input)
	if(input > speed) then
		return speed
	elseif(input < -speed) then
		return -Speed
	else
		return input
	end
end
local function onKeyEvent(event)
	local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    print( message )
	-- 눌린 상태
	if(event.phase == "down") then
		if (event.keyName == "up") then
			move_y = signInv(move_y + speed)
		elseif (event.keyName == "down") then
			move_y = signInv(move_y - speed)
		elseif (event.keyName == "left") then 
			move_x = signInv(move_x - speed)
		elseif (event.keyName == "right") then 
			move_x = signInv(move_x + speed)
		elseif(event.keyName == "space" and isEnd == 0) then 
			-- field.me.attack[#field.me.attack + 1] = addBall(field.me.display.x, field.me.display.y, 0)
			table.insert(field.me.attack, 1, addBall(field.me.display.x, field.me.display.y, 0))
		end
	elseif(event.phase == "up") then
		if (event.keyName == "up") then
			move_y = signInv(move_y - speed)
		elseif (event.keyName == "down") then
			move_y = signInv(move_y + speed)
		elseif (event.keyName == "left") then 
			move_x = signInv(move_x + speed)
		elseif (event.keyName == "right") then 
			move_x = signInv(move_x - speed)
		end
	end
    return false
end

-- enemy value setting
field["enemy"] = {}
field.enemy["time"] = 100
field.enemy["attack"] = {}
field.enemy["x"] = display.contentCenterX
field.enemy["y"] = 50
field.enemy["attackNum"] = 0
field.enemy["hp"] = ENEMY_HP
field.enemy["sizeX"] = 100 
field.enemy["sizeY"] = 50
field.enemy["display"] = display.newRect(field.enemy.x, field.enemy.y, field.enemy.sizeX, field.enemy.sizeY)
field.enemy["hpView"] = display.newText({text=field.enemy.hp,fontSize=20,x=field.enemy.display.x,y=field.enemy.display.y})
field.enemy.display:setFillColor(1,0,0,1)

-- My value setting
field["me"] = {}
field.me["attack"] = {}
field.me["x"] = display.contentCenterX
field.me["y"] = display.contentHeight - 50
field.me["sizeX"] = 50 
field.me["sizeY"] = 50
field.me["display"] = display.newRect(field.me.x, field.me.y, field.me.sizeX, field.me.sizeY)
field.me["attackNum"] = 0
field.me["hp"] = MY_HP

-- http://onebyonedesign.com/flash/particleeditor/
-- Create the emitter

local inGame = function()
	if #field.enemy.attack ~= 0 and isEnd ~= 1 then
		print("Enemy Attack : " .. #field.enemy.attack)
		local loop = #field.enemy.attack
		for i = 1, loop, 1 do
			if (isEnd == 1) then 
				display.remove(field.enemy.attack[i])
				table.remove(field.enemy.attack, i)
				i, loop = i - 1, loop - 1
			elseif(field.enemy.attack[i] ~= nil) then
				if(field.me.x == nil) then 
					print("me.x nil")
				elseif(field.me.y == nil) then
					print("me.y nil")
				-- elseif(field.enemy.attack[i].x == nil) then 
					-- print("field.enemy.attack[i].x nil")
				elseif(field.enemy.attack[i].y == nil) then
					print("field.enemy.attack[i].y nil")
				elseif(field.enemy.attack[i].x >= field.me.x - (field.me.sizeX / 2) 
						and field.enemy.attack[i].x <= field.me.x + (field.me.sizeX / 2)) 
					and (field.enemy.attack[i].y >= field.me.y - (field.me.sizeY / 2) 
						and field.enemy.attack[i].y <= field.me.y + (field.me.sizeY / 2)) then
					display.remove(field.enemy.attack[i])
					table.remove(field.enemy.attack, i)
					i, loop = i - 1, loop - 1
					field.me.hp = field.me.hp - 1
					print("i'm hit")
					print(field.me.hp)
					if(field.me.hp == 0) then
						display.remove(field.me.display)
						display.remove(field.enemy.display)
						display.remove(field.enemy.hpView)
						local emitter = display.newEmitter(particle)
						emitter.x = display.contentCenterX
						emitter.y = display.contentCenterY
						emitter.anchorY = 1
						isEnd = 1
					end
						-- field.enemy.hpView.text = field.enemy.hp
				elseif(field.enemy.attack[i].y >= display.contentHeight) then
					-- print(#field.me.attack)
					display.remove(field.enemy.attack[i])
					table.remove(field.enemy.attack, i)
					-- print(#field.me.attack)
					i, loop = i - 1, loop - 1
				else
					field.enemy.attack[i].y = field.enemy.attack[i].y + 36
				end
			end
		end
	end
	
	if #field.me.attack ~= 0 and isEnd ~= 1 then
		print("Attack Count : " .. #field.me.attack)
		local loop = #field.me.attack
		for i = 1, loop, 1 do
			if (isEnd == 1) then 
				display.remove(field.me.attack[i])
				table.remove(field.me.attack)
				i, loop = i - 1, loop - 1
			elseif(field.me.attack[i] ~= nil) then
				if(field.me.attack[i].x >= field.enemy.display.x - (field.enemy.sizeX / 2) 
						and field.me.attack[i].x <= field.enemy.display.x + (field.enemy.sizeX / 2)) 
					and (field.me.attack[i].y >= field.enemy.display.y - (field.enemy.sizeY / 2) 
						and field.me.attack[i].y <= field.enemy.display.y + (field.enemy.sizeY / 2)) then
					display.remove(field.me.attack[i])
					table.remove(field.me.attack)
					i, loop = i - 1, loop - 1
					field.enemy.hp = field.enemy.hp - 1
					if(field.enemy.hp == 0) then
						display.remove(field.me.display)
						display.remove(field.enemy.display)
						display.remove(field.enemy.hpView)
						local emitter = display.newEmitter(particle)
						emitter.x = display.contentCenterX
						emitter.y = display.contentCenterY
						emitter.anchorY = 1
						isEnd = 1
					end
						field.enemy.hpView.text = field.enemy.hp
				elseif(field.me.attack[i].y <= 0) then
					-- print(#field.me.attack)
					display.remove(field.me.attack[i])
					table.remove(field.me.attack)
					-- print(#field.me.attack)
					i, loop = i - 1, loop - 1
				else
					field.me.attack[i].y = field.me.attack[i].y - 36
				end
			end
		end
	elseif(isEnd == 1 and textEnd == 0) then 
		local loop = #field.me.attack
		for i = 1, loop, 1 do
			display.remove(field.me.attack[i])
			table.remove(field.me.attack, i)
			i, loop = i - 1, loop - 1
		end
		
		loop = #field.enemy.attack
		for i = 1, loop, 1 do
			display.remove(field.enemy.attack[i])
			table.remove(field.enemy.attack, i)
			i, loop = i - 1, loop - 1
		end
		if(field.me.hp ~= 0) then
			resultTextView = display.newText({text="Clear!!!",font = native.systemFontBold,fontSize=100,x=display.contentCenterX,y=display.contentCenterY})
		else
			resultTextView = display.newText({text="Fail...",font = native.systemFontBold,fontSize=100,x=display.contentCenterX,y=display.contentCenterY})	
		end
		print("My attack finish : " .. #field.me.attack)
		if #field.me.attack ~= 0 then				
			loop = #field.me.attack
			for i = 1, loop, 1 do
				display.remove(field.me.attack[i])
			end
			for i = 1, loop, 1 do
				table.remove(field.me.attack)
			end
		end
		print("Enemy attack finish : " .. #field.enemy.attack)
		if #field.enemy.attack ~= 0 then				
			loop = #field.enemy.attack
			for i = 1, loop, 1 do
				display.remove(field.enemy.attack[i])
			end
			for i = 1, loop, 1 do
				table.remove(field.enemy.attack)
			end
		end
		resultTextView:setFillColor(1,1,1,1)
		resultTextView.anchorY = 0
		transition.animText(resultTextView, {time=1000, transition = easing.linear})
		textEnd = 1
	end
	
	if(field.me.display.x ~= nil and field.me.display.y ~= nil) then 
		if(field.me.display.x + move_x <= display.contentWidth and field.me.display.x + move_x >= 0) then
			if(field.me.display.y + move_y >= display.contentHeight or field.me.display.y + move_y <= 0) then
				field.me.display.y = display.contentHeight - 50
			else
				field.me.display.x, field.me.display.y = field.me.display.x + move_x, field.me.display.y - move_y
				field.me.x, field.me.y = field.me.display.x, field.me.display.y 
			end
		end
	end
	
end

Runtime:addEventListener("enterFrame", inGame)

local function spawnBall( randomPosition )
	if(isEnd ~= 1) then
		local x = math.random(display.contentWidth)
		transition.moveTo( field.enemy.display, { x=x, y=field.enemy.display.y, time=field.enemy.time } )
		transition.moveTo( field.enemy.hpView, { x=x, y=field.enemy.display.y, time=field.enemy.time } )
		table.insert(field.enemy.attack, 1, addBall(x, field.enemy.display.y, 1))
	end
end

-- Obtain a random number for the spawned object's x position

-- Wrap "spawnBall" and "randomPosition" inside a closure
local myClosure = function()
	return spawnBall( randomPosition ) 
end
timer.performWithDelay(field.enemy.time, myClosure, -1 )

Runtime:addEventListener( "key", onKeyEvent )