-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

local x, y = display.contentCenterX, display.contentCenterY
-- 원 사이즈
local size = 10
-- 이동 좌표
local move_x, move_y = 0, 0 
local circle = display.newCircle(x,y,size)

-- 영역들 소리
Sounds = {}
Sounds.upLeft = audio.loadSound("upLeft.mp3")
Sounds.upRight = audio.loadSound("upRight.mp3")
Sounds.downLeft = audio.loadSound("downLeft.mp3")
Sounds.downRight = audio.loadSound("downRight.mp3")
Sounds.nowPlay = nil
-- 각 영역들이 저장될 변수
Area = {}

local signInv = function(input)
	if(input > 5) then
		return 5
	elseif(input < -5) then
		return -5
	else
		return input
	end
end

local function onKeyEvent(event)
	-- local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    -- print( message )
	-- 눌린 상태
	if(event.phase == "down") then
		if (event.keyName == "up") then
			move_y = signInv(move_y + 5)
		elseif (event.keyName == "down") then
			move_y = signInv(move_y - 5)
		elseif (event.keyName == "left") then 
			move_x = signInv(move_x - 5)
		elseif (event.keyName == "right") then 
			move_x = signInv(move_x + 5)
		elseif(event.keyName == "space") then 
			circle.x, circle.y = display.contentCenterX, display.contentCenterY
		end
	elseif(event.phase == "up") then
		if (event.keyName == "up") then
			move_y = signInv(move_y - 5)
		elseif (event.keyName == "down") then
			move_y = signInv(move_y + 5)
		elseif (event.keyName == "left") then 
			move_x = signInv(move_x + 5)
		elseif (event.keyName == "right") then 
			move_x = signInv(move_x - 5)
		end
	end
    return false
end

-- 화면 그리는 부분
local drawArea = function ()
	local centerX, centerY = 300, 300
	local sizeX, sizeY = centerX, centerY
	Area.upLeftArea = display.newImageRect("upLeft.png", centerX, centerY)
	Area.upLeftArea.x, Area.upLeftArea.y = sizeX / 2, sizeY / 2
	Area.upLeftArea.alpha = 0.5	
	-- display.newText("Crack Traxxxx", sizeX / 2, sizeY / 2, native.systemFontBold, 26)
	
	Area.upRightArea = display.newImageRect("upRight.png", centerX, centerY)
	Area.upRightArea.x, Area.upRightArea.y = sizeX / 2 + centerX, sizeY / 2
	Area.upRightArea.alpha = 0.5	
	-- display.newText("Max Burning", sizeX / 2 + centerX, sizeY / 2, native.systemFontBold, 26)
	
	Area.downLeftArea = display.newImageRect("downLeft.jpg", centerX, centerY)
	Area.downLeftArea.x, Area.downLeftArea.y = sizeX / 2, sizeY / 2 + centerY
	Area.downLeftArea.alpha = 0.5	
	-- display.newText("파피푸", sizeX / 2, sizeY / 2 + centerY, native.systemFontBold, 26)
	
	Area.downRightArea = display.newImageRect("downRight.jpg", centerX, centerY)
	Area.downRightArea.x, Area.downRightArea.y = sizeX / 2 + centerX, sizeY / 2 + centerY
	Area.downRightArea.alpha = 0.5	
	-- display.newText("도파민", sizeX / 2 + centerX, sizeY / 2 + centerY, native.systemFontBold, 26)
end 

local count = 0
-- count 를 이용해서 1초마다 한번씩만 변수 출력
-- 매 초마다 계산
Runtime:addEventListener("enterFrame",function()
	circle.x,circle.y = circle.x + move_x,circle.y - move_y
	--print("Position Info")
	--print("Circle X Value : " .. circle.x)
	--	print("Circle Y Value : " .. circle.y)
	if(circle.x == display.contentCenterX or circle.y == display.contentCenterY) then
		--print("This position is border")
		Area.upLeftArea.alpha = 0.5
		Area.upRightArea.alpha = 0.5
		Area.downLeftArea.alpha = 0.5
		Area.downRightArea.alpha = 0.5
		if (Sounds.nowPlay ~= nil) then
			print("All song stop!")
			audio.pause(Sounds.nowPlay)
			Sounds.nowPlay = nil
		end
	-- 좌측 상단
	elseif(circle.x < display.contentCenterX and circle.y < display.contentCenterY) then 
		--print("This position is the top left")
		Area.upLeftArea.alpha = 1
		Area.upRightArea.alpha = 0.5
		Area.downLeftArea.alpha = 0.5
		Area.downRightArea.alpha = 0.5
		if Sounds.nowPlay == nil then 
			print("Play : Crack Traxxxx")
			Sounds.nowPlay = audio.play(Sounds.upLeft, { loops=-1 } )
		end
	-- 우측 상단
	elseif(circle.x > display.contentCenterX and circle.y < display.contentCenterY) then 
		--print("This position is the top right")
		Area.upRightArea.alpha = 1
		Area.upLeftArea.alpha = 0.5
		Area.downLeftArea.alpha = 0.5
		Area.downRightArea.alpha = 0.5
		if Sounds.nowPlay == nil then 
			print("Play : Max burning!!!")
			Sounds.nowPlay = audio.play(Sounds.upRight, { loops=-1 } )
		end
	-- 좌측 하단
	elseif(circle.x < display.contentCenterX and circle.y > display.contentCenterY) then 
		--print("This position is the bottom left")
		Area.downLeftArea.alpha = 1
		Area.upLeftArea.alpha = 0.5
		Area.upRightArea.alpha = 0.5
		Area.downRightArea.alpha = 0.5
		if Sounds.nowPlay == nil then 
			print("Play : パ→ピ→プ→Yeah!")
			Sounds.nowPlay = audio.play(Sounds.downLeft, { loops=-1 } )
		end
	-- 우측 하단
	elseif(circle.x > display.contentCenterX and circle.y > display.contentCenterY) then 
		--print("This position is the bottom right")
		Area.downRightArea.alpha = 1
		Area.upLeftArea.alpha = 0.5
		Area.upRightArea.alpha = 0.5
		Area.downLeftArea.alpha = 0.5
		if Sounds.nowPlay == nil then  
			print("Play : 도파민")
			Sounds.nowPlay = audio.play(Sounds.downRight, { loops=-1 } )
		end
	
	end
end)

Runtime:addEventListener("key", onKeyEvent)
drawArea()