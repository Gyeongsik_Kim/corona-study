-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
display.setDefault( "anchorX", 0.5 )
display.setDefault( "anchorY", 0.5 )

local x,y = display.contentCenterX, display.contentCenterY
-- local o = display.newRect( x, y, display.contentWidth, display.contentHeight )
local o = display.newRect( x, y, 1280, 720) -- x, y, width, height
o.fill = { type="image", filename="wallhaven-201968.jpg" }
o.fill.scaleX = 1
o.fill.scaleY = 1

local sound = audio.loadSound("test.mp3")
local soundChannel = audio.play(sound)

