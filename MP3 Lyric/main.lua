-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here


transition.animText = function(subj, prop)
	local retTrans
	if type(subj) == "table" and type(subj.text) == "string" and type(subj._proxy) == "userdata" then
		local index = {idx=0}
		local origText = subj.text
		local runtimeAdded = function()
			if(prop.addRandText ~= false) then
				subj.text = origText:sub(1,index.idx)..math.random(48,122)
			else
				subj.text = origText:sub(1,index.idx)
			end
		end
		local onCancelled = function()
			if prop.onCancelled ~= nil then
				prop.onCancelled(prop)
			end
			subj.text = origText
			Runtime:removeEventListener("enterFrame",runtimeAdded)
		end
		local onComplete = function()
			onCancelled()
			if prop.onComplete ~= nil then
				prop.onComplete(prop)
			end
			Runtime:removeEventListener("enterFrame",runtimeAdded)
		end
		local onStart = function()
			if prop.onStart ~= nil then
				prop.onStart(prop)
			end
			Runtime:addEventListener("enterFrame",runtimeAdded)
		end
		retTrans = transition.to(index,{time=(prop.time or 500),idx=subj.text:len(),onCancelled=onCancelled,onComplete=onComplete,easing=prop.transition,delay=prop.delay,onStart=onStart,onRepeat=prop.onRepeat})
		subj.text = ""
	end
	return retTrans
end

local function lyricGetTime(t) 
	return ((tonumber(t:sub(2,3)) * 60 * 100) +  (tonumber(t:sub(5,6)) * 100) + (tonumber(t:sub(8,9))))
end

local function lyricGetLyric(t) 
	return t:sub(11)
end 

string.split = function(inputstr, sep)
	if inputstr == nil then 
		return nil
	end
	if sep == nil then
		sep = "%s"
	end
	local t={}
	local i=1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end


local socket = require("socket")
start = socket.gettime()*1000
function getTime()
	return socket.gettime()*1000 - start
	-- 950 = 0.95초
	-- 1초 = 1000
end

 
local lyrics = {}
local parsingData = string.split(require("lyric"),"\r\n")
local sound = audio.loadSound("test.mp3")
-- local parsingData = string.split(require("bareriko"),"\r\n")
-- local sound = audio.loadSound("bareriko.wma")

lyricsView = nil
audio.play(sound, {loop = 0})
print(start)


for k,v in pairs(parsingData) do
	print(k .. ", Value : ".. v)
	if lyrics[lyricGetTime(v)] ~= nil then
		lyrics[lyricGetTime(v)] = lyrics[lyricGetTime(v)] .. "\n" .. lyricGetLyric(v)
	else 
		lyrics[lyricGetTime(v)] = lyricGetLyric(v)
	end
end

for k,v in pairs(lyrics) do 
	print(k .. ", Value : ".. v)
end
-- local circle = display.newCircle( 100, 100, 40 )
-- circle:setFillColor( 0, 0, 1 )
-- transition.to( circle, { time=400, y=200, transition=easing.linear } )
i = 0
Runtime:addEventListener("enterFrame", function()
	for i = i, getTime() / 10, 1 do
		if(lyrics[i] ~= nil) then 
			print(lyrics[i])
			if lyricsView ~= nil then
				display.remove(lyricsView)
			end
				lyricsView = display.newText({text=lyrics[i],fontSize=20,x=display.contentCenterX,y=display.contentCenterY})
				transition.animText( lyricsView, { time=1000, transition=easing.linear})
				lyrics[i] = nil
		end
	end
end)


-- local background = display.newImageRect("background.png", 640, 640)
-- background.x, background.y, background.alpha = 320, 320, 1
