-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

-- 출발 지점 (화면 중앙)
local x, y = display.contentCenterX, display.contentCenterY
-- 원 사이즈
local size = 100
-- 이동 좌표
local move_x, move_y = 0, 0 
local circle = display.newCircle(x,y,100)
local flag_x, flag_y = 0, 0
local signInv = function(input)
	if(input > 5) then
		return 5
	elseif(input < -5) then
		return -5
	else
		return input
	end
end

local function onKeyEvent(event)
	local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    print( message )
	-- 눌린 상태
	if(event.phase == "down") then
		if (event.keyName == "up") then
			move_y = signInv(move_y + 5)
		elseif (event.keyName == "down") then
			move_y = signInv(move_y - 5)
		elseif (event.keyName == "left") then 
			move_x = signInv(move_x - 5)
		elseif (event.keyName == "right") then 
			move_x = signInv(move_x + 5)
		elseif(event.keyName == "space") then 
			circle.x, circle.y = display.contentCenterX, display.contentCenterY
		end
	elseif(event.phase == "up") then
		if (event.keyName == "up") then
			move_y = signInv(move_y - 5)
		elseif (event.keyName == "down") then
			move_y = signInv(move_y + 5)
		elseif (event.keyName == "left") then 
			move_x = signInv(move_x + 5)
		elseif (event.keyName == "right") then 
			move_x = signInv(move_x - 5)
		end
	end
    return false
end


local upline = display.newLine(0,0,display.contentWidth, 0)
upline:setStrokeColor(1, 1, 1, 1)
upline.strokeWidth = 8

local leftline = display.newLine(0,0,0, display.contentHeight)
leftline:setStrokeColor(1, 1, 1, 1)
leftline.strokeWidth = 8

local rightline = display.newLine(display.contentWidth,0,display.contentWidth, display.contentHeight)
rightline:setStrokeColor(1, 1, 1, 1)
rightline.strokeWidth = 8

local underline = display.newLine(0, display.contentHeight,display.contentWidth, display.contentHeight)
underline:setStrokeColor(1, 1, 1, 1)
underline.strokeWidth = 8

Runtime:addEventListener("enterFrame",function()
	if((circle.x <= display.contentWidth - size and circle.x >= size and 
		circle.y <= display.contentHeight - size and circle.y >= size)) then
		circle.x,circle.y = circle.x + move_x,circle.y - move_y
	end
end)

Runtime:addEventListener( "key", onKeyEvent )
local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
            -- Do nothing; dialog will simply dismiss
        elseif ( i == 2 ) then
            -- Open URL if "Learn More" (second button) was clicked
            system.openURL( "http://www.coronalabs.com" )
        end
    end
end

-- Show alert with two buttons
-- local alert = native.showAlert( "Corona", "Dream. Build. Ship.", { "OK", "Learn More" }, onComplete )