function table.copy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[table.copy(orig_key)] = table.copy(orig_value)
        end
        setmetatable(copy, table.copy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function table.print(inputTable)
	for key, value in pairs(inputTable) do
		if type(value) == "table" then
			functionCallCount = functionCallCount + 1
			print(string.rep(("  "),functionCallCount-1)..key.." = ".."{")
			printTable(value)
			functionCallCount = functionCallCount - 1
			print(string.rep(("  "),functionCallCount).."}")
		else
			print(string.rep(("  "),functionCallCount)..key.." = "..value)
		end
	end
end

function math.sign(v)
	if(v > 0) then
		return 1
	elseif(v < 0) then
		return -1
	else
		return 0
	end
end

function math.absolute(v)
	if(v<0) then
		return -v
	else
		return v
	end
end

function math.signInv(v)
	if(v >= 0) then
		return -1
	elseif(v < 0) then
		return 1
	end
end

string.split = function(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={} ; i=1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

local function networkTimeListener(event)
	if(event.isError) then
		return -1
	else
		return tonumber(event.response)
	end
end

function getNetworkTime(listener)
	network.request("http://xmillis.com/api/v2/millis-since-unix-epoch.php", "GET", function(event)
		listener(networkTimeListener(event))
	end)
end

function trim(s)
	local ret = (s:gsub("^%s*(.-)%s*$", "%1"))
	return ret
end

local function doesFileExist( fname, path )

    local results = false

    local filePath = system.pathForFile( fname, path )

    if ( filePath ) then
        filePath = io.open( filePath, "r" )
    end

    if ( filePath ) then
        filePath:close()
        results = true
    end

    return results
end

function readDocument(filename)
	local lf = io.open(system.pathForFile(filename, system.DocumentsDirectory ) , "r")
	local ret
	if lf then
		ret = lf:read("*a")
		lf:close()
	end
	return ret
end

local performWithDelayOrig = timer.performWithDelay
timer.performWithDelay3 = function(...)
	local args = {...}
	local tm = performWithDelayOrig(args[1],args[2],args[3] or 1)
	if(#args > 3) then
		tm.params = args[4]
	end
	args = nil
	return tm
end

function copyFile( srcName, srcPath, dstName, dstPath, overwrite )

    local results = false

    if not ( overwrite ) then
        if ( doesFileExist( dstName, dstPath ) ) then
            return 1
        end
    end

    local rfilePath = system.pathForFile( srcName )
    local wfilePath = system.pathForFile( dstName, dstPath )

    local rfh = io.open( rfilePath, "rb" )
    local wfh = io.open( wfilePath, "wb" )

    if not ( wfh ) then
        return false
    else
        local data = rfh:read( "*a" )
        if not ( data ) then
            return false
        else
            if not ( wfh:write( data ) ) then
                return false
            end
        end
    end

    results = 2

    rfh:close()
    wfh:close()

    return results
end

transition.animText = function(subj, prop)
	local retTrans
	if type(subj) == "table" and type(subj.text) == "string" and type(subj._proxy) == "userdata" then
		local index = {idx=0}
		local origText = subj.text
		local runtimeAdded = function()
			if(prop.addRandText ~= false) then
				subj.text = origText:sub(1,index.idx)..math.random(48,122)
			else
				subj.text = origText:sub(1,index.idx)
			end
		end
		local onCancelled = function()
			if prop.onCancelled ~= nil then
				prop.onCancelled(prop)
			end
			subj.text = origText
			Runtime:removeEventListener("enterFrame",runtimeAdded)
		end
		local onComplete = function()
			onCancelled()
			if prop.onComplete ~= nil then
				prop.onComplete(prop)
			end
			Runtime:removeEventListener("enterFrame",runtimeAdded)
		end
		local onStart = function()
			if prop.onStart ~= nil then
				prop.onStart(prop)
			end
			Runtime:addEventListener("enterFrame",runtimeAdded)
		end
		retTrans = transition.to(index,{time=(prop.time or 500),idx=subj.text:len(),onCancelled=onCancelled,onComplete=onComplete,easing=prop.transition,delay=prop.delay,onStart=onStart,onRepeat=prop.onRepeat})
		subj.text = ""
	end
	return retTrans
end